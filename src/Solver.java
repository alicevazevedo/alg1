import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/****************************************************************************
 *  Week 4
 *  Compilation:  javac Solver.java
 *
 *  Solver.
 *
 ****************************************************************************/


/**
 * 
 * @author aliceazevedo
 *
 */
public class Solver {
	
	private static final int STEPS = 1;
	
	private boolean solverRan;
	private SearchNode initialSN;
	private SearchNode twinSN;
	private SearchNode solutionSN;
	private MinPQ<SearchNode> pq = new MinPQ<SearchNode>();
	private MinPQ<SearchNode> twinPq = new MinPQ<SearchNode>();
	
	// find a solution to the initial board (using the A* algorithm)
    public Solver(Board initialBoard) {
    	this.initialSN = new SearchNode(initialBoard);
    	this.pq.insert(this.initialSN);
    	this.twinSN = new SearchNode(this.initialSN.board.twin());
    	twinPq.insert(this.twinSN);
    }
    
    // is the initial board solvable?
    /** 
     * To detect such situations, use the fact that boards are divided into two equivalence classes with respect to reachability: 
     * (i) those that lead to the goal board and 
     * (ii) those that lead to the goal board if we modify the initial board by swapping any pair of adjacent (non-blank) blocks in the same row. 
     * (Difficult challenge for the mathematically inclined: prove this fact.) 
     * To apply the fact, run the A* algorithm simultaneously on two puzzle instances
     * one with the initial board 
     * and one with the initial board modified by swapping a pair of adjacent blocks in the same row. 
     * Exactly one of the two will lead to the goal board.
     * @return
     */
    public boolean isSolvable() {
    	if (!this.solverRan) {
    		solution();
    	}
    	return this.solutionSN.board != null;
    }
    
    // min number of moves to solve initial board; -1 if no solution
    public int moves() {
    	if (!this.solverRan) {
    		solution();
    	}
    	return this.solutionSN.moves;
    }
    
    // sequence of boards in a shortest solution; null if no solution
    public Iterable<Board> solution() {
    	if (!this.solverRan) {
    		this.solverRan = true;
    		
        	SearchNode solution = null;
        	boolean mainPq = true;
        	while (solution == null) {
        		int i = 0;
        		while (i < STEPS) {
        			if (mainPq) {
        				solution = step(this.pq);
        			} else {
        				solution = step(this.twinPq);
        			}
        			if (solution != null) {
        				if (!mainPq)
        					solution = noSolution();
        				
    					this.solutionSN = solution;
    					break;
        			}
        			i++;
        		}
        		mainPq = !mainPq;
        	}
    	}
    	Stack<Board> stack = new Stack<Board>();
    	SearchNode node = this.solutionSN;
    	while (node != null) {
    		if (node.board != null)
    			stack.push(node.board);
    		node = node.previous;
    	}
    	
    	if (stack.isEmpty()) 
    		return null;
    	
    	return stack;
    }
    
    /**
     *  We define a search node of the game to be a board, the number of moves made to reach the board, and the previous search node. 
     *  First, insert the initial search node (the initial board, 0 moves, and a null previous search node) into a priority queue. 
     *  
     *  Then, delete from the priority queue the search node with the minimum priority, and insert onto the priority queue all neighboring search nodes 
     *  (those that can be reached in one move from the dequeued search node). 
     *  
     *  Repeat this procedure until the search node dequeued corresponds to a goal board. 
     *  The success of this approach hinges on the choice of priority function for a search node. 
     *  
     *  To solve the puzzle from a given search node on the priority queue, the total number of moves we need to make (including those already made) 
     *  is at least its priority, using either the Hamming or Manhattan priority function. 
     *  (For Hamming priority, this is true because each block that is out of place must move at least once to reach its goal position. 
     *  For Manhattan priority, this is true because each block must move its Manhattan distance from its goal position. 
     *  Note that we do not count the blank square when computing the Hamming or Manhattan priorities.) 
     *  
     *  Consequently, when the goal board is dequeued, we have discovered not only a sequence of moves from the initial board to the goal board, 
     *  but one that makes the fewest number of moves. (Challenge for the mathematically inclined: prove this fact.)
     * @param priorityQueue
     * @return
     */
	private SearchNode step(MinPQ<SearchNode> priorityQueue) {
		if (!priorityQueue.isEmpty()) {
    		SearchNode current = priorityQueue.delMin();
	    	if (current.board.isGoal()) {
	    		return current;
	    	}
	    	
	    	Iterator<SearchNode> neighbors = current.neighbors().iterator();
	    	while (neighbors.hasNext()) {
	    		SearchNode neighbor = neighbors.next();
    			if ((current.previous != null && current.previous.equals(neighbor)) 
					) {
//    				|| nodesInPQ.contains(neighbor) ) {
	    			continue;
	    		}
	    		
    			priorityQueue.insert(neighbor);
//    			nodesInPQ.add(neighbor);
	    	}
    	}
		return null;
	}
    
    /**
     * 
     * @return
     */
    private SearchNode noSolution() {
    	SearchNode node = new SearchNode(null);
    	node.moves = -1;
    	return node;
    }
    
    private class SearchNode implements Comparable<SearchNode> {
    	int moves;
    	private Board board;
    	private SearchNode previous;
    	
    	private SearchNode(Board board) {
    		this.board = board;
    	}
    	
    	private Iterable<SearchNode> neighbors() {
    		Queue<SearchNode> queue = new Queue<SearchNode>();
    		for (Board board : this.board.neighbors()) {
    			SearchNode searchNode = new SearchNode(board);
    			searchNode.moves = this.moves + 1;
	    		searchNode.previous = this;
	    		queue.enqueue(searchNode);
    		}
    		return queue;
    	}
    	
    	private int priority() {
    		return this.board.manhattan() + this.moves;
    	}
    	
		@Override
		public int compareTo(SearchNode o) {
			if (this.priority() == o.priority()) {
				if (this.board.manhattan() < o.board.manhattan()) {
					return 1;
				}
				return 0;
			}
			if (this.priority() < o.priority()) {
				return -1;
			}
			return 1;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (obj instanceof SearchNode) {
				SearchNode other = (SearchNode) obj;
				if (this.board != null && other.board != null)
					return this.board.equals(other.board);
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			if (this.board == null) return -1;
			return this.board.hashCode();
		}
    }
    
    // solve a slider puzzle (given below)
    public static void main(String[] args) {
    	// create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);
        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}