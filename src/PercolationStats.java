/****************************************************************************
 *  Week 1
 *  Compilation:  javac PercolationStats.java
 *  Dependencies: Percolation.java StdStats.java
 *
 *  Percolation.
 *
 ****************************************************************************/

/**
 * The <tt>Percolation</tt> class represents Given a composite systems comprised
 * of randomly distributed insulating and metallic materials: what fraction of
 * the materials need to be metallic so that the composite system is an
 * electrical conductor? Given a porous landscape with water on the surface (or
 * oil below), under what conditions will the water be able to drain through to
 * the bottom (or the oil to gush through to the surface)? Scientists have
 * defined an abstract process known as percolation to model such situations.
 * 
 * @author Alice Azevedo
 */
public class PercolationStats {

	private double[] openSites; // array of fraction of open sites
	
	private final static double FACTOR = 1.96; // factor to calculate confidence

	/**
	 * Initializes a PercolationStats.
	 * 
	 * @param N
	 *            N-by-N grids
	 * @param T
	 *            independent computational experiments
	 * @throws IllegalArgumentException
	 *             when N or T are negative
	 */
	public PercolationStats(int N, int T) {
		if (N <= 0 || T <= 0) {
			throw new IllegalArgumentException("either N or T is less than 0");
		}
		openSites = new double[T];
		for (int e = 0; e < T; e++) {
			Percolation percolation = new Percolation(N);
			int openSites = 0;

			while (!percolation.percolates()) {
				int i = StdRandom.uniform(1, N + 1);
				int j = StdRandom.uniform(1, N + 1);
				if (percolation.isOpen(i, j)) {
					continue;
				}
				percolation.open(i, j);
				openSites++;
			}
			this.openSites[e] = openSites / (Math.pow(N, 2));
			openSites = 0;
		}
	}

	/**
	 * @return sample mean of percolation threshold
	 */
	public double mean() {
		return StdStats.mean(openSites);
	}

	/**
	 * @return sample standard deviation of percolation threshold
	 */
	public double stddev() {
		return StdStats.stddev(this.openSites);
	}

	/**
	 * @return lower bound of the 95% confidence interval
	 */
	public double confidenceLo() {
		double mean = mean();
		double factorStddev = FACTOR * stddev();

		return mean - (factorStddev / Math.sqrt(this.openSites.length));
	}

	/**
	 * @return upper bound of the 95% confidence interval
	 */
	public double confidenceHi() {
		double mean = mean();
		double factorStddev = FACTOR * stddev();

		return mean + (factorStddev / Math.sqrt(this.openSites.length));
	}

	/**
	 * test client, described below
	 * 
	 * @param args
	 *            expects two integers, N and T
	 */
	public static void main(String[] args) {
		int N = StdIn.readInt();
		int T = StdIn.readInt();

		PercolationStats stats = new PercolationStats(N, T);

		System.out.println("mean\t\t\t= " + stats.mean());
		System.out.println("stddev\t\t\t= " + stats.stddev());
		System.out.println("95% confidence interval\t= " + stats.confidenceLo()
				+ ", " + stats.confidenceHi());
	}
}
