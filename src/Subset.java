/****************************************************************************
 *  Week 2
 *  Compilation:  javac Subset.java
 *
 *  Subset.
 *
 ****************************************************************************/



/**
 * A client program that takes a command-line integer k; 
 * reads in a sequence of N strings from standard input using StdIn.readString(); 
 * and prints out exactly k of them, uniformly at random. 
 * Each item from the sequence can be printed out at most once. 
 * You may assume that k >= 0 and no greater than the number of string N on standard input.
 * 
 * @author aliceazevedo
 *
 */
public class Subset {
	private RandomizedQueue<String> rq = new RandomizedQueue<String>();
	
	/**
	 * unit test
	 * @param args
	 */
	public static void main(String[] args) {
		int k = Integer.valueOf(args[0]);
		if (k < 0) {
			System.exit(0);
		}
		Subset subset = new Subset();
		while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            subset.rq.enqueue(item);
        }
		
		while (k > 0) {
			System.out.println(subset.rq.dequeue());
			k--;
		}
	}
}