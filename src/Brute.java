/****************************************************************************
 *  Week 3
 *  Compilation:  javac Brute.java
 *
 *  Brute.
 *
 ****************************************************************************/
import java.util.Arrays;


/**
 * Brute.java examines 4 points at a time and checks whether they all lie on the same line segment, 
 * printing out any such line segments to standard output and drawing them using standard drawing. 
 * To check whether the 4 points p, q, r, and s are collinear, check whether the slopes between p and q, between p and r, and between p and s are all equal.
 * 
 * The order of growth of the running time of your program should be N4 in the worst case and it should use space proportional to N.
 * 
 * @author Alice Azevedo
 *
 */
public class Brute {
	
	public static void main(String[] args) {
		String fileName = args[0];
		In in = new In(fileName);
		int N = in.readInt();
		
		StdDraw.setXscale(0, 32768);
		StdDraw.setYscale(0, 32768);
		
		Point[] points = new Point[N];
		for (int i = 0; i < N; i++) {
			int x = in.readInt();
			int y = in.readInt();
			Point p = new Point(x, y);
			p.draw();
			points[i] = p;
		}
		
		Arrays.sort(points);
		
		// p1 p2 p3 p4 p5
		// 0  1  2  3  4
		for (int i = 0; i < N - 3; i++) {
			Point p = points[i];
			for (int j = i + 1; j < N -2; j++) {
				Point q = points[j];
				double qSlope = p.slopeTo(q);
				
				for (int k = j+1; k < N -1; k++) {
					Point r = points[k];
					double rSlope = p.slopeTo(r);
					
					if (rSlope != qSlope) {
						continue;
					}
					
					for (int l = k + 1; l < N; l++) {
						Point s = points[l];
						double sSlope = p.slopeTo(s);
						
						if (qSlope == rSlope && rSlope == sSlope) {
							p.drawTo(s);
							StdOut.printf("%s -> %s -> %s -> %s \n", p, q, r, s);
						}
					}
				}
			}
		}
	}

}