/****************************************************************************
 *  Week 2
 *  Compilation:  javac RandomizedQueue.java
 *
 *  RandomizedQueue.
 *
 ****************************************************************************/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A randomized queue is similar to a stack or queue, 
 * except that the item removed is chosen uniformly at 
 * random from items in the data structure.
 * @author aliceazevedo
 *
 * @param <Item>
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
	private Item[] array; // data structure in array
	private int N; // number of nodes
	private int lastIdx = -1; // index of last used spot in the array
	
	/**
	 * Constructs a new RandomizedQueue of size two
	 */
	@SuppressWarnings("unchecked")
	public RandomizedQueue() {
		array = (Item[]) new Object[2];
	}
	
	/**
	 * Is there any nodes?
	 * @return if the deque is empty
	 */
	public boolean isEmpty() {
		return N == 0;
	}
	
	/**
	 * How many nodes are in the Deque
	 * @return size of nodes
	 */
	public int size() {
		return N;
	}
	
	/**
	 * Adds the item to the queue
	 * @param item
	 * @throws NullPointerException if item is null
	 */
	public void enqueue(Item item) {
		if (item == null) {
			throw new NullPointerException("null is not accepted");
		}
		if (N == array.length) {
			// double size of array if necessary
			resize(2 * array.length);
		}
		lastIdx++;
        array[N++] = item;
	}
	
	/**
	 * resizes the array
	 * @param capacity
	 */
	private void resize(int capacity) {
        assert capacity >= N;
        @SuppressWarnings("unchecked")
		Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < N; i++) {
            temp[i] = array[i];
        }
        array = temp;
    }
	
	/**
	 * delete and return a random item
	 * @return
	 * @throws NullPointerException if item is null
	 */
	public Item dequeue() {
		if (isEmpty()) {
			throw new NoSuchElementException("No item left");
		}
		int idx = uniformIndex(lastIdx);
		Item item = array[idx];
		// copia o ultimo elemento para a nova 'vaga' aberta
		array[idx] = array[lastIdx];
		// atualiza o valor da ultima posicao para null, e ultima posicao aponta para anterior
		// to avoid loitering
		array[lastIdx--] = null;
        N--;
        
        // shrink size of array if necessary
        if (N > 0 && N == array.length/4) {
        	resize(array.length/2);
        }
        return item;
	}
	
	/**
	 * return (but do not delete) a random item
	 * @return
	 * @throws NullPointerException if item is null
	 */
	public Item sample() {
		if (isEmpty()) {
			throw new NoSuchElementException("No item left");
		}
		int idx = uniformIndex(lastIdx);
		return array[idx];
	}
	
	private int uniformIndex(int number) {
		return StdRandom.uniform(number + 1);
	}

	@Override
	public Iterator<Item> iterator() {
		return new RandomizedQueueIterator();
	}
	
	public static void main(String[] args) {
		RandomizedQueue<String> rq = new RandomizedQueue<String>();
		for (int i = 0; i < 1000; i++) {
			rq.enqueue(Integer.toString(i));
		}
		System.out.println(rq.size());
		
		Iterator<String> it1 = rq.iterator();
		Iterator<String> it2 = rq.iterator();
		ArrayList<String> a1 = new ArrayList<String>();
		int count = 0;
		while(it1.hasNext()) {
			String item = it1.next();
			System.out.print(item + " ");
			a1.add(item);
			count++;
		}
		System.out.println("\ncount: " + count);
		System.out.println();
		count = 0;
		while(it2.hasNext()) {
			String item = it2.next();
			if(!a1.contains(item)) {
				System.out.println("erro");
				throw new RuntimeException("item nao encontrado: " + item);
			} else {
				a1.remove(item);
			}
			count++;
			System.out.print(item + " ");
		}
		System.out.println("\ncount: " + count);
		System.out.println("\nsucesso");
	}
	
	/**
	 * Class used to iterate over the queue
	 * @author aliceazevedo
	 *
	 */
	private class RandomizedQueueIterator implements Iterator<Item> {
		private int size = N;
		private int itemsLeft = N;
		private boolean[] used = new boolean[N];
		
		public boolean hasNext() {
			return itemsLeft > 0;
		}
		
		@Override
		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException("No item left");
			}
			int randomIdx = StdRandom.uniform(size);
			while (used[randomIdx]) {
				randomIdx = StdRandom.uniform(size);
			}
			Item item = array[randomIdx];
			used[randomIdx] = true;
			itemsLeft--;
			return item;
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}