/****************************************************************************
 *  Week 2
 *  Compilation:  javac Deque.java
 *
 *  Deque.
 *
 ****************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A double-ended queue or deque (pronounced "deck") is 
 * a generalization of a stack and a queue that supports inserting and 
 * removing items from either the front or the back of the data structure. 
 * 
 * @author Alice Azevedo
 * 
 * @param <Item>
 */
public class Deque<Item> implements Iterable<Item> {
	
	private Node first; // pointer to the first node
	private Node last; // pointer to the last node
	private int N; // number of nodes
	
	/**
	 * Holds the Node structure
	 * @author aliceazevedo
	 *
	 */
	private class Node {
		private Item value; // holds the value
		private Node next; // pointer to the next node
		private Node previous; // pointer to the previous node
		
		/**
		 * Constructs a node
		 * @param value with this value
		 */
		Node (Item value) {
			this.value = value;
		}
		
		@Override
		public String toString() {
			return value.toString();
		}
	}
	
	/**
	 * Construct an empty deque
	 */
	public Deque() {
	}
	
	/**
	 * Is there any nodes?
	 * @return if the deque is empty
	 */
	public boolean isEmpty() {
		return N == 0;
	}
	
	/**
	 * How many nodes are in the Deque
	 * @return size of nodes
	 */
	public int size() {
		return N;
	}
	
	/**
	 * Adds the item in the beginning
	 * @param item
	 * @throws NullPointerException if item is null
	 */
	public void addFirst(Item item) {
		if (item == null) {
			throw new NullPointerException("null is not accepted");
		}
		Node newFirst = new Node(item);
		newFirst.next = this.first;
//		this.first = newFirst;
		if (isEmpty()) {
			this.last = newFirst;
		} else {
			this.first.previous = newFirst;
		}
		this.first = newFirst;
		N++;
	}
	
	/**
	 * Adds the item in the end
	 * @param item
	 * @throws NullPointerException if item is null
	 */
	public void addLast(Item item) {
		if (item == null) {
			throw new NullPointerException("null is not accepted");
		}
		Node newLast = new Node(item);
		if (isEmpty()) {
			this.first = newLast;
			this.last = newLast;
		} else {
			newLast.previous = this.last;
			this.last.next = newLast;
			this.last = newLast;
		}
		N++;
	}
	
	/**
	 * removes the item from the beginning
	 * @return first node
	 * @throws NoSuchElementException if deque is empty
	 */
	public Item removeFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException("No item left");
		}
		Node node = this.first;
		this.first = node.next;
		if (this.first != null) {
			this.first.previous = null;
		}
		N--;
		if (isEmpty()) {
			this.last = null;
		}
		return node.value;
	}
	
	/**
	 * removes the item from the end
	 * @return last node
	 * @throws NoSuchElementException if deque is empty
	 */
	public Item removeLast() {
		if (isEmpty()) {
			throw new NoSuchElementException("No item left");
		}
		
		Item value = this.last.value;
		Node newLast = this.last.previous;
		if (newLast != null) {
			newLast.next = null;
		}
		this.last = newLast;
		N--;
		if (isEmpty()) {
			this.first = null;
		}
		return value;
	}

	@Override
	public Iterator<Item> iterator() {
		return new ItemIterator();
	}
	
	public static void main(String[] args) {
		Deque<Integer> deque = new Deque<Integer>();
        /*for (int j = 0; j < 5; j++) {
            deque.addLast(j);
        }
        StdOut.println(deque.size());*/
        /*Iterator<Integer> i = deque.iterator();
        while (i.hasNext()) {
        	System.out.print(i.next() + " ");
        }*/
        /*while (!deque.isEmpty()) {
        	System.out.println(deque.removeLast());
        }*/
		
		deque.addFirst(1);
		deque.addLast(2);
		deque.removeFirst();
		deque.removeLast();
		Iterator<Integer> it = deque.iterator();
		System.out.println(it.next());
        
	}
	
	/**
	 * Class used to iterate in the Deque
	 * @author aliceazevedo
	 *
	 */
	private class ItemIterator implements Iterator<Item> {
		private Node current = first;
		
		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException("No more objects to iterate through");
			}
			Item item = current.value;
			current = current.next;

			return item;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

}