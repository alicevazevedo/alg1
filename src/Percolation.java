/****************************************************************************
 *  Week 1
 *  Compilation:  javac Percolation.java
 *  Dependencies: WeightedQuickUnionUF.java
 *
 *  Percolation.
 *
 ****************************************************************************/

/**
 * The <tt>Percolation</tt> class represents Given a composite systems comprised
 * of randomly distributed insulating and metallic materials: what fraction of
 * the materials need to be metallic so that the composite system is an
 * electrical conductor? Given a porous landscape with water on the surface (or
 * oil below), under what conditions will the water be able to drain through to
 * the bottom (or the oil to gush through to the surface)? Scientists have
 * defined an abstract process known as percolation to model such situations.
 * 
 * @author Alice Azevedo
 */
public class Percolation {

	private WeightedQuickUnionUF wQuickUnion; // auxiliary WeightedQuickUnionUF
	private WeightedQuickUnionUF wQuickUnion2; // auxiliary WeightedQuickUnionUF
	private boolean[] sites; // array of sites
	private int size; // size of full sites
	private int N; // N-dimension grid
	
	/**
	 * Initializes a Percolation with closed sites.
	 * 
	 * @param N
	 *            the number of objects
	 */
	public Percolation(int N) {
		if (N <= 0)
			throw new IllegalArgumentException("N must be postive");
		int size = (N * N); // number of sites for a percolate grid
		size += 2; // room for auxiliary sites: one for the top to connect to
					// the top row and the other to connect to the bottom row
		this.size = size;
		this.N = N;
		wQuickUnion = new WeightedQuickUnionUF(size);
		wQuickUnion2 = new WeightedQuickUnionUF(size);
		sites = new boolean[size];

		sites[topSiteIndex()] = true;
		sites[bottomSiteIndex()] = true;

		// connect top virtual sites to top row
		for (int j = 1; j <= N; j++) {
			wQuickUnion.union(topSiteIndex(), getIndex(1, j));
			wQuickUnion2.union(topSiteIndex(), getIndex(1, j));
		}
	}

	/**
	 * Open the site at i,j
	 * 
	 * @param i
	 *            the integer representing row
	 * @param j
	 *            the integer representing column
	 */
	public void open(int i, int j) {
		if (i <= 0 || i > N)
			throw new IndexOutOfBoundsException("row index i out of bounds");
		if (j <= 0 || j > N)
			throw new IndexOutOfBoundsException("col index j out of bounds");

		if (!isOpen(i, j)) {
			
			int index = getIndex(i, j);
			
			if (i == N) {
				// site in bottom row, connect it to bottom virtual site
				wQuickUnion.union(bottomSiteIndex(), getIndex(i, j));
			}
			
			// set the site as open
			sites[index] = true;

			// connect it to open neighbors
			int topNeighborIndex = topNeighborIndex(i, j);
			if (topNeighborIndex != -1) {
				if (sites[topNeighborIndex]) {
					wQuickUnion.union(index, topNeighborIndex);
					wQuickUnion2.union(index, topNeighborIndex);
				}
			}

			int bottomNeighborIndex = bottomNeighborIndex(i, j);
			if (bottomNeighborIndex != -1) {
				if (sites[bottomNeighborIndex]) {
					wQuickUnion.union(index, bottomNeighborIndex);
					wQuickUnion2.union(index, bottomNeighborIndex);
				}
			}

			int leftNeighorIndex = leftNeighborIndex(i, j);
			if (leftNeighorIndex != -1) {
				if (sites[leftNeighorIndex]) {
					wQuickUnion.union(index, leftNeighorIndex);
					wQuickUnion2.union(index, leftNeighorIndex);
				}
			}

			int rightNeighorIndex = rightNeighborIndex(i, j);
			if (rightNeighorIndex != -1) {
				if (sites[rightNeighorIndex]) {
					wQuickUnion.union(index, rightNeighorIndex);
					wQuickUnion2.union(index, rightNeighorIndex);
				}
			}
		}
	}

	/**
	 * Is the site at i,j opened?
	 * 
	 * @param i
	 *            the integer representing row
	 * @param j
	 *            the integer representing column
	 * @return <tt>true</tt> if is open and <tt>false</tt> otherwise
	 */
	public boolean isOpen(int i, int j) {
		if (i <= 0 || i > N)
			throw new IndexOutOfBoundsException("row index i out of bounds");
		if (j <= 0 || j > N)
			throw new IndexOutOfBoundsException("col index j out of bounds");
		return sites[getIndex(i, j)];
	}

	/**
	 * Is site (row i, column j) full? A full site is an open site that can be
	 * connected to an open site in the top row via a chain of neighboring
	 * (left, right, up, down) open sites.
	 * 
	 * @param i
	 *            the integer representing row
	 * @param j
	 *            the integer representing column
	 * @return <tt>true</tt> if is full and <tt>false</tt> otherwise
	 */
	public boolean isFull(int i, int j) {
		if (i <= 0 || i > N)
			throw new IndexOutOfBoundsException("row index i out of bounds");
		if (j <= 0 || j > N)
			throw new IndexOutOfBoundsException("col index j out of bounds");
		if (isOpen(i, j)) {
			return wQuickUnion2.connected(getIndex(i, j), topSiteIndex());
		}
		return false;
	}

	/**
	 * Does the system percolate?
	 * 
	 * @return <tt>true</tt> if is full and <tt>false</tt> otherwise
	 */
	public boolean percolates() {
		return wQuickUnion.connected(topSiteIndex(), bottomSiteIndex());
	}

	// return the top site index in quick find array
	private int topSiteIndex() {
		return 0;
	}

	// return the bottom site index in quick find array
	private int bottomSiteIndex() {
		return this.size - 1;
	}

	// By convention, the indices i and j are integers between 1 and N, where
	// (1, 1) is the upper-left site
	private int getIndex(int i, int j) {
		// (i - 1) * N + j
		if (i <= 0 || j <= 0 || i >= size || j >= size) {
			throw new IndexOutOfBoundsException(
					"either i or j is outside the range 1-" + N);
		}
		return ((i - 1) * N) + j;
	}

	// return the index for the top neighor of element i, j
	private int topNeighborIndex(int i, int j) {
		if (i == 1) {
			return -1;
		}
		return getIndex(i, j) - N;
	}

	// return the index for the bottom neighor of element i, j
	private int bottomNeighborIndex(int i, int j) {
		if (i == N) {
			return -1;
		}
		return getIndex(i, j) + N;
	}

	// return the index for the left neighor of element i, j
	private int leftNeighborIndex(int i, int j) {
		if (j == 1) {
			return -1;
		}
		return getIndex(i, j) - 1;
	}

	// return the index for the right neighor of element i, j
	private int rightNeighborIndex(int i, int j) {
		if (j == N) {
			return -1;
		}
		return getIndex(i, j) + 1;
	}

	public static void main(String[] args) {
		int N = 4;
		Percolation current = new Percolation(N);

		print(current);

		while (!current.percolates()) {
			System.out.println("entre i e j");
			int i = StdIn.readInt();
			int j = StdIn.readInt();
			if (current.isOpen(i, j)) {
				continue;
			}
			current.open(i, j);
			print(current);
			System.out.println("opening " + i + "," + j);
			System.out.println(current.isFull(i, j));
		}
	}

	private static void print(Percolation p) {
		for (int i = 1; i <= p.N; i++) {
			for (int j = 1; j <= p.N; j++) {
				System.out.print(p.sites[p.getIndex(i, j)]);
				System.out.print(" ");
			}
			System.out.println();
		}
	}

}