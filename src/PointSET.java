// week 5
import java.util.ArrayList;
import java.util.Iterator;


public class PointSET {

	private SET<Point2D> set;
	
	// construct an empty set of points
	public PointSET() {
		this.set = new SET<Point2D>();
	}
	
	// is the set empty?
	public boolean isEmpty() {
		return this.set.isEmpty();
	}
	
	// number of points in the set
	public int size() {
		return this.set.size();
	}
	
	// add the point p to the set (if it is not already in the set)
	public void insert(Point2D p) {
		if (p == null) throw new IllegalArgumentException("null is an invalid argument");
		this.set.add(p);
	}
	
	// does the set contain the point p?
	public boolean contains(Point2D p) {
		if (p == null) throw new IllegalArgumentException("null is an invalid argument");
		return this.set.contains(p);
	}
	
	// draw all of the points to standard draw
	public void draw() {
		Iterator<Point2D> iterator = this.set.iterator();
		while (iterator.hasNext()) {
			Point2D p = iterator.next();
			p.draw();
		}
	}
	
	// all points in the set that are inside the rectangle
	public Iterable<Point2D> range(RectHV rect) {
		ArrayList<Point2D> result = new ArrayList<Point2D>();
		Iterator<Point2D> iterator = this.set.iterator();
		while (iterator.hasNext()) {
			Point2D p = iterator.next();
			if (rect.contains(p)) {
				result.add(p);
			}
		}
		return result;
	}
	
	// a nearest neighbor in the set to p; null if set is empty
	public Point2D nearest(Point2D p) {
		if (p == null) throw new IllegalArgumentException("null is an invalid argument");
		if (isEmpty()) return null;
		
		double smallestDistance = Double.MAX_VALUE;
		Point2D n = null;
		Iterator<Point2D> iterator = this.set.iterator();
		while (iterator.hasNext()) {
			Point2D curr = iterator.next();
			double distance = p.distanceSquaredTo(curr);
			if (distance < smallestDistance) {
				smallestDistance = distance;
				n = curr;
			}
		}
		
		return n;
	}
}
