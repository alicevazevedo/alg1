/*************************************************************************
 * Name:
 * Email:
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
	/**
	 * The SLOPE_ORDER comparator should compare points by the slopes they make with the invoking point (x0, y0). 
	 * Formally, the point (x1, y1) is less than the point (x2, y2) if and only if the slope (y1 − y0) / (x1 − x0) 
	 * is less than the slope (y2 − y0) / (x2 − x0). 
	 * Treat horizontal, vertical, and degenerate line segments as in the slopeTo() method.
	 */
    public final Comparator<Point> SLOPE_ORDER = new PointSlopeComparator(); // slope comparator

    private final int x; // x coordinate
    private final int y; // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    /**
     * The slopeTo() method should return the slope between the invoking point (x0, y0) and the argument point (x1, y1), 
     * which is given by the formula (y1 − y0) / (x1 − x0). Treat the slope of a horizontal line segment as positive zero; 
     * treat the slope of a vertical line segment as positive infinity; 
     * treat the slope of a degenerate line segment (between a point and itself) as negative infinity.
     * @param that
     * @return
     */
    public double slopeTo(Point that) {
    	if (that == null) {
    		throw new NullPointerException();
    	}
    	
    	// Treat the slope of a degenerate line segment (between a point and itself) as negative infinity.
    	if (that.compareTo(this) == 0) {
    		return Double.NEGATIVE_INFINITY;
    	}
    	
    	// Treat the slope of a horizontal line segment as positive zero
    	if (that.y == this.y) {
    		return 0;
    	}
    	
    	// Treat the slope of a vertical line segment as positive infinity
    	if (that.x == this.x) {
    		return Double.POSITIVE_INFINITY;
    	}
    	
    	Double dividend = new Double(that.y - this.y);
    	if (dividend == 0) {
    		return 0.0;
    	}
    	
    	Double divider = new Double(that.x - this.x);
    	if (divider == 0) {
    		if (dividend < 0) {
    			return Double.NEGATIVE_INFINITY;
    		}
    		return Double.POSITIVE_INFINITY;
    	}
    	
    	return dividend / divider;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    /**
     * The compareTo() method should compare points by their y-coordinates, breaking ties by their x-coordinates. 
     * Formally, the invoking point (x0, y0) is less than the argument point (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     */
    public int compareTo(Point that) {
    	if (this.y < that.y) {
    		return -1;
    	} else if (this.y > that.y){
    		return 1;
    	} else {
    		if (this.x < that.x) {
    			return -1;
    		} else if (this.x > that.x) {
    			return 1;
    		}
    		return 0;
    	}
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        /* YOUR CODE HERE */
    	Point p = new Point(390,  466);
    	Point q = new Point(390, 466);
    	double slope = p.slopeTo(q);
    	System.out.println(slope);
    }
    
    /**
     * The SLOPE_ORDER comparator should compare points by the slopes they make with the invoking point (x0, y0). 
     * Formally, the point (x1, y1) is less than the point (x2, y2) if and only if 
     * the slope (y1 − y0) / (x1 − x0) is less than the slope (y2 − y0) / (x2 − x0). 
     * Treat horizontal, vertical, and degenerate line segments as in the slopeTo() method.
     * @author Alice Azevedo
     *
     */
    private class PointSlopeComparator implements Comparator<Point>
    {
        public int compare(final Point o1, final Point o2)
        {
        	if (o1 == null || o2 == null) throw new NullPointerException();

            final double slope1 = slopeTo(o1);
            final double slope2 = slopeTo(o2);

            if (slope1 < slope2) return -1;
            if (slope1 > slope2) return +1;
            return 0;
        }
    }
}