/****************************************************************************
 *  Week 3
 *  Compilation:  javac Fast.java
 *
 *  Fast.
 *
 ****************************************************************************/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A faster, sorting-based solution. Remarkably, it is possible to solve the problem much faster than the brute-force solution described above. Given a point p, the following method determines whether p participates in a set of 4 or more collinear points.
 * 
 * Think of p as the origin.
 * For each other point q, determine the slope it makes with p.
 * Sort the points according to the slopes they makes with p.
 * Check if any 3 (or more) adjacent points in the sorted order have equal slopes with respect to p. If so, these points, together with p, are collinear.
 * Applying this method for each of the N points in turn yields an efficient algorithm to the problem. 
 * The algorithm solves the problem because points that have equal slopes with respect to p are collinear, and sorting brings such points together. 
 * The algorithm is fast because the bottleneck operation is sorting.
 * 
 * @author Alice Azevedo
 * 
 */
public class Fast {
	
	public static void main(String[] args) {
		String fileName = args[0];
		In in = new In(fileName);
		int N = in.readInt();
		
		StdDraw.setXscale(0, 32768);
		StdDraw.setYscale(0, 32768);
		
		Point[] points = new Point[N];
		for (int i = 0; i < N; i++) {
			int x = in.readInt();
			int y = in.readInt();
			Point p = new Point(x, y);
			points[i] = p;
			p.draw();
		}
		// sort points according to axes
		Arrays.sort(points);
		for (int i = 0; i < N - 3; i++) {
			Point origin = points[i];
			
			// copy points
			Point[] copyOfPoints = Arrays.copyOfRange(points, 0, N);
			// sort them according to slope with origin
			Arrays.sort(copyOfPoints, origin.SLOPE_ORDER);
			
			int step = 1;
			for (int j = 0; j < copyOfPoints.length; j+=step, step = 1) {
				Point q = copyOfPoints[j];
				
				// calculate the slope
				double currentSlope = origin.slopeTo(q);
				List<Point> slices = new ArrayList<Point>();
				StringBuilder builder = new StringBuilder();
				
				// find every point with same slope
				while (j + step < copyOfPoints.length && currentSlope == origin.slopeTo(copyOfPoints[j+step])) {
					if (step == 1) {
						slices.add(q);
						builder.append(" -> ");
						builder.append(q);
					}
					builder.append(" -> ");
					builder.append(copyOfPoints[j+step]);
					slices.add(copyOfPoints[j+step]);
					step++;
				}
				if (step >= 3) {
					// sort slices
					Collections.sort(slices);
					// if origin is not the smallest point (axes), segment has already been visited, discard it.
					if (origin.compareTo(slices.get(0)) > 0) {
						continue;
					} else {
						// else, found new segment
						builder.insert(0, origin);
						StdOut.println(builder);
						origin.drawTo(slices.get(slices.size() -1));
					}
				}
			}
		}
	}
	
}
