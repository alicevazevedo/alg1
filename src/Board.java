import java.util.Arrays;

/****************************************************************************
 *  Week 4
 *  Compilation:  javac Board.java
 *
 *  Board.
 *
 ****************************************************************************/


/**
 * We define a search node of the game to be a board, the number of moves made to reach the board, and the previous search node. 
 * @author aliceazevedo
 *
 */
public class Board {
	
	private int N; // board dimension
	private final int[][] blocks; // holds the board data
	private int hamming; // The number of blocks in the wrong position 
	private int manhattan; // The sum of the Manhattan distances (sum of the vertical and horizontal distance) from the blocks to their goal positions
	
	private int emptyRow; // holds information about empty spot
	
	// construct a board from an N-by-N array of blocks
	// (where blocks[i][j] = block in row i, column j)
	public Board(int[][] blocks) {
		this.blocks = copyBlocks(blocks);
		this.N = blocks[0].length;
		for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
            	int correctValue = (i * this.N) + (j + 1); // value the position should be holding
            	int curValue = blocks[i][j];
            	if (curValue != 0 && correctValue != curValue) {
            		this.hamming++;
            		
            		int row = (curValue -1) / this.N;
            		int col = (curValue -1) % this.N;
            		this.manhattan += Math.abs(row - i) + Math.abs(col - j);
            	}
            	if (curValue == 0) {
            		emptyRow = i;
            	}
            }
		}
	}
	
	// board dimension N
	public int dimension() {
		return this.N;
	}
	
	// number of blocks out of place
	public int hamming() {
		return this.hamming;
	}
	
	
	// sum of Manhattan distances between blocks and goal
	public int manhattan() {
		return this.manhattan;
	}
	
	// is this board the goal board?
	public boolean isGoal() {
		return this.manhattan == 0;
	}
	
	// a board obtained by exchanging two adjacent blocks in the same row
	public Board twin() {
		int row = 0;
		int col = 0;
		int adj = col + 1;
		
		if (emptyRow == row) {
			if (emptyRow == this.N - 1)
				row = emptyRow - 2;
			else 
				row = emptyRow + 1;
		}
		if (adj >= this.N) {
			throw new RuntimeException("No adjacent block");
		}
		
		int[][] twin = copyBlocks(this.blocks);
		twin[row][col] = this.blocks[row][adj];
		twin[row][adj] = this.blocks[row][col];
		return new Board(twin);
	}
	
	private int[][] copyBlocks(int[][] original) {
		int length = original.length;
		int[][] copy = new int[length][length];
		for (int i = 0; i < length; i++) {
			copy[i] = Arrays.copyOf(original[i], original[i].length);
		}
		return copy;
	}
	
	// does this board equal y?
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (obj instanceof Board) {
			Board other = (Board) obj;
			return Arrays.deepEquals(this.blocks, other.blocks);
		}
		return false;
	}
		
	// all neighboring boards
	// (those that can be reached in one move from the dequeued search node)
	public Iterable<Board> neighbors() {
		Queue<Board> queue = new Queue<Board>();
		int row = -1;
		int col = -1;
		for (int i = 0; i < this.N; i++) {
			for (int j = 0; j < this.N; j++) {
				if (isEmpty(i, j)) {
					row = i;
					col = j;
					break;
				}
			}
		}
		if (canMoveDownTo(row, col)) {
			int[][] copy = copyBlocks(this.blocks);
			copy[row][col] = copy[row - 1][col];
			copy[row - 1][col] = 0;
			queue.enqueue(neighbour(copy));
		}
		if (canMoveUpTo(row, col)) {
			int[][] copy = copyBlocks(this.blocks);
			copy[row][col] = copy[row + 1][col];
			copy[row + 1][col] = 0;
			queue.enqueue(neighbour(copy));
		}
		if (canMoveFromLeftTo(row, col)) {
			int[][] copy = copyBlocks(this.blocks);
			copy[row][col] = copy[row][col + 1];
			copy[row][col + 1] = 0;
			queue.enqueue(neighbour(copy));
		}
		if (canMoveFromRightTo(row, col)) {
			int[][] copy = copyBlocks(this.blocks);
			copy[row][col] = copy[row][col - 1];
			copy[row][col - 1] = 0;
			queue.enqueue(neighbour(copy));
		}
		
		return queue;
	}
	
	/**
	 * 
	 * @param row of empty spot
	 * @param col of empty spot
	 * @return if upper piece can be moved down to (row, col)
	 */
	private boolean canMoveDownTo(int row, int col) {
		if (!isEmpty(row, col)) {
			throw new IllegalArgumentException("row and col does not designate an empty spot");
		}
		return row != 0;
	}
	
	/**
	 * 
	 * @param row of empty spot
	 * @param col of empty spot
	 * @return if upper piece can be moved down to (row, col)
	 */
	private boolean canMoveUpTo(int row, int col) {
		if (!isEmpty(row, col)) {
			throw new IllegalArgumentException("row and col does not designate an empty spot");
		}
		return row != this.N - 1;
	}
	
	/**
	 * 
	 * @param row of empty spot
	 * @param col of empty spot
	 * @return if piece from right can be moved to (row, col)
	 */
	private boolean canMoveFromRightTo(int row, int col) {
		if (!isEmpty(row, col)) {
			throw new IllegalArgumentException("row and col does not designate an empty spot");
		}
		return col != 0;
	}

	/**
	 * 
	 * @param row of empty spot
	 * @param col of empty spot
	 * @return if piece from left can be moved to (row, col)
	 */
	private boolean canMoveFromLeftTo(int row, int col) {
		if (!isEmpty(row, col)) {
			throw new IllegalArgumentException("row and col does not designate an empty spot");
		}
		return col != this.N - 1;
	}
	
	/**
	 * Create a new board, neighbour of the current.
	 * @param items
	 * @return
	 */
	private Board neighbour(int[][] items) {
		Board board = new Board(items);
		return board;
	}
		
	// string representation of the board (in the output format specified below)
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.N + "\n");
	    for (int i = 0; i < this.N; i++) {
	        for (int j = 0; j < this.N; j++) {
	            builder.append(String.format("%2d ", this.blocks[i][j]));
	        }
	        builder.append("\n");
	    }
	    return builder.toString();
	}
	
	/**
	 * 
	 * @param row
	 * @param col
	 * @return is the spot is 0
	 */
	private boolean isEmpty(int row, int col) {
		return this.blocks[row][col] == 0;
	}
	
	public static void main(String[] args) {
		In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
//        Board other = new Board(blocks);
//        StdOut.println(initial.equals(other));
        
//        StdOut.println(initial);
//        StdOut.println(initial.hamming());
//        StdOut.println(initial.manhattan());
        
        StdOut.println(initial);
        StdOut.println(initial.twin());
        
        /*for (Board n : initial.neighbors()) {
        	StdOut.println(n);
        }*/
        
	}
}