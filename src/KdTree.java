import java.io.File;
import java.util.ArrayList;

public class KdTree {
	
	private Node root; // first node
	private int N; // size

	// construct an empty set of points
	public KdTree() {
	}
	
	// is the set empty?
	public boolean isEmpty() {
		return this.N == 0;
	}
	
	// number of points in the set
	public int size() {
		return this.N;
	}
	
	// add the point p to the set (if it is not already in the set)
	public void insert(Point2D p) {
		if (p == null) throw new IllegalArgumentException("null is an invalid argument");
		
		if (this.contains(p)) return;
		
		build(p, true);
	}
	
	private Node build(Point2D p, boolean insert) {
		Node n = new Node();
//		n.p = p;
		n.x = p.x();
		n.y = p.y();
		
		// at the root we use the x-coordinate (if the point to be inserted has a smaller x-coordinate than the point at the root, go left; otherwise go right); 
		// then at the next level, we use the y-coordinate (if the point to be inserted has a smaller y-coordinate than the point in the node, go left; otherwise go right); 
		// then at the next level the x-coordinate, and so forth.
		Node parent = null;
		Node s = root;
		boolean xLevel = true;
		boolean parentLeft = false;
		
		while (s != null) {
			parent = s;
			if (xLevel) {
				if (p.x() < s.x) {
					// left
					s = s.lb;
					parentLeft = true;
				} else {
					//right
					s = s.rt;
					parentLeft = false;
				}
			} else {
				if (p.y() < s.y) {
					// left
					s = s.lb;
					parentLeft = true;
				} else {
					// right
					s = s.rt;
					parentLeft = false;
				}
			}
			xLevel = !xLevel;
		}
		
		n.rect = rect(parent, xLevel, parentLeft);
		
		if (insert) {
			if (this.isEmpty()) {
				root = n;
			} else {
				if (parentLeft) {
					parent.lb = n;
				} else {
					parent.rt = n;
				}
			}
			this.N++;
		}
		
		return n;
	}
	
	// returns rectangle
	private RectHV rect(Node parent, boolean xLevel, boolean parentLeft) {
		if (parent == null) return new RectHV(0, 0, 1, 1);
		
		RectHV result = null;
		
		if (xLevel) { // left or right
			if (parentLeft) { // bottom
				result = new RectHV(parent.rect.xmin(), parent.rect.ymin(), parent.rect.xmax(), parent.y);
			} else { // top
				result = new RectHV(parent.rect.xmin(), parent.y, parent.rect.xmax(), parent.rect.ymax());
			}
		} else { // bottom or top
			if (parentLeft) { // left
				result = new RectHV(parent.rect.xmin(), parent.rect.ymin(), parent.x, parent.rect.ymax());
			} else { // right
				result = new RectHV(parent.x, parent.rect.ymin(), parent.rect.xmax(), parent.rect.ymax());
			}
		}
		
		return result;
	}
	
	// does the set contain the point p?
	public boolean contains(Point2D p) {
		if (p == null) throw new IllegalArgumentException("null is an invalid argument");
		
		Node s = root;
		boolean xLevel = true;
		
		while (s != null) {
			if (xLevel) {
				if (p.x() == s.x && p.y() == s.y) {
					return true;
				}
				if (p.x() < s.x) {
					// left
					s = s.lb;
				} else {
					//right
					s = s.rt;
				}
			} else {
				if (p.x() == s.x && p.y() == s.y) {
					return true;
				}
				if (p.y() < s.y) {
					// left
					s = s.lb;
				} else {
					// right
					s = s.rt;
				}
			}
			xLevel = !xLevel;
		}
		
		return false;
	}
	
	// draw all of the points to standard draw
	public void draw() {
		StdDraw.clear();
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius();
		RectHV r = new RectHV(0, 0, 1, 1);
		r.draw();
		
		draw(root, null, false, false);
	}
	
	private RectHV splittingLine(Node node, Node parent, boolean xAxis, boolean leftBottom) {
		RectHV rect = null;
		if (node == this.root) {
			rect = new RectHV(node.x, node.rect.ymin(), node.x, node.rect.ymax());
		} else {
			if (xAxis) {
				if (leftBottom)
					rect = new RectHV(node.rect.xmin(), node.y, parent.x, node.y);
				else
					rect = new RectHV(parent.x, node.y, node.rect.xmax(), node.y);
			} else {
				if (leftBottom)
					rect = new RectHV(node.x, node.rect.ymin(), node.x, parent.y);
				else
					rect = new RectHV(node.x, parent.y, node.x, node.rect.ymax());
			}
		}
		return rect;
	}
	
	private void draw(Node node, Node parent, boolean xAxis, boolean leftBottom) {
		if (node == null) return;
		
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius(.01);
		Point2D point = new Point2D(node.x, node.y);
		point.draw();
		
		StdDraw.setPenRadius();
		
		RectHV line = splittingLine(node, parent, xAxis, leftBottom);
		
		if (xAxis) {
			StdDraw.setPenColor(StdDraw.BLUE);
		} else {
			StdDraw.setPenColor(StdDraw.RED);
		}
		System.out.println("[" + line.xmin() + ", " + line.ymin() + "] x [" + line.xmax() + ", " + line.ymax() + "]");
		StdDraw.line(line.xmin(), line.ymin(), line.xmax(), line.ymax());
		
		if (node.lb != null) {
			draw(node.lb, node, !xAxis, true);
		}
		if (node.rt != null) {
			draw(node.rt, node, !xAxis, false);
		}
		
	}
	
	// all points in the set that are inside the rectangle
	public Iterable<Point2D> range(RectHV rect) {
		ArrayList<Point2D> result = new ArrayList<Point2D>();
		if (this.root != null) {
			Queue<Node> q = new Queue<KdTree.Node>();
			q.enqueue(this.root);
			
			while(!q.isEmpty()) {
				Node n = q.dequeue();
				Point2D point = new Point2D(n.x, n.y);
				if (rect.intersects(n.rect)) {
					if (rect.contains(point)) {
						result.add(point);
					}
					if (n.lb != null && rect.intersects(n.lb.rect)) {
						q.enqueue(n.lb);
					}
					if (n.rt != null && rect.intersects(n.rt.rect)) {
						q.enqueue(n.rt);
					}
				}
			}
		}
		return result;
	}
	
	private Point2D nearestHelper(Node node, RectHV rect, double x, double y, Point2D nearestPointCandidate, boolean isVertical) {

        Point2D nearestPoint = nearestPointCandidate;

        if (node != null) {
            RectHV leftRect = null;
            RectHV rightRect = null;
            Point2D queryPoint = new Point2D(x, y);

            if (nearestPoint == null || queryPoint.distanceSquaredTo(nearestPoint) > rect.distanceSquaredTo(queryPoint)) {

                Point2D nodePoint = new Point2D(node.x, node.y);

                if (nearestPoint == null) {
                    nearestPoint = nodePoint;
                } else {
                    if (queryPoint.distanceSquaredTo(nearestPoint) > queryPoint.distanceSquaredTo(nodePoint)) {
                        nearestPoint = nodePoint;
                    }
                }

                if (isVertical) {
                    leftRect = new RectHV(rect.xmin(), rect.ymin(), node.x, rect.ymax());
                    rightRect = new RectHV(node.x, rect.ymin(), rect.xmax(), rect.ymax());
                    if (x <= node.x) {
                        nearestPoint = nearestHelper(node.lb, leftRect, x, y, nearestPoint, !isVertical);
                        nearestPoint = nearestHelper(node.rt, rightRect, x, y, nearestPoint, !isVertical);
                    } else {
                        nearestPoint = nearestHelper(node.rt, rightRect, x, y, nearestPoint, !isVertical);
                        nearestPoint = nearestHelper(node.lb, leftRect, x, y, nearestPoint, !isVertical);
                    }
                } else {
                    leftRect = new RectHV(rect.xmin(), rect.ymin(), rect.xmax(), node.y);
                    rightRect = new RectHV(rect.xmin(), node.y, rect.xmax(), rect.ymax());
                    if (y <= node.y) {
                        nearestPoint = nearestHelper(node.lb, leftRect, x, y, nearestPoint, !isVertical);
                        nearestPoint = nearestHelper(node.rt, rightRect, x, y, nearestPoint, !isVertical);
                    } else {
                        nearestPoint = nearestHelper(node.rt, rightRect, x, y, nearestPoint, !isVertical);
                        nearestPoint = nearestHelper(node.lb, leftRect, x, y, nearestPoint, !isVertical);
                    }
                }
            }
        }
        return nearestPoint;
    }

    public Point2D nearest(Point2D p) {
    	if (this.isEmpty()) return null;
        return nearestHelper(this.root, this.root.rect, p.x(), p.y(), null, true);
    }
	
	private static class Node {
		private double x;
		private double y;
		private RectHV rect; // the axis-aligned rectangle corresponding to this node
		private Node lb; // the left/bottom subtree
		private Node rt; // the right/top subtree
	}
	
	private void print() {
		Queue<Node> q = new Queue<Node>();
		q.enqueue(root);
		
		int c = 1;
		while (!q.isEmpty()) {
			Node n = q.dequeue();
			c--;
			
			if (n.lb != null) q.enqueue(n.lb);
			if (n.rt != null) q.enqueue(n.rt);
			
			if (c == 0) {
				System.out.print(n.x + ", " + n.y);
				c = q.size();
				System.out.println();
			}
		}
	}
	
	public static void main(String[] args) {
		
		KdTree k = new KdTree();
		/*
		k.insert(new Point2D(18, 6));
		k.insert(new Point2D(2, 17));
		k.insert(new Point2D(0, 14));
		k.insert(new Point2D(10, 3));
		k.insert(new Point2D(13, 0));
		k.insert(new Point2D(4, 5));
		k.insert(new Point2D(12, 2));
		k.insert(new Point2D(3, 4));
		*/
		/*
		k.insert(new Point2D(0.87, 0.06));
		k.insert(new Point2D(0.84, 0.25));
		k.insert(new Point2D(0.94, 0.97));
		k.insert(new Point2D(0.86, 0.46));
		k.insert(new Point2D(0.57, 0.12));
		k.insert(new Point2D(0.67, 0.78));
		k.insert(new Point2D(0.03, 0.98));
		k.insert(new Point2D(0.40, 0.11));
		*/
//		k.print();
		k.insert(new Point2D(5, 32));
		k.insert(new Point2D(8, 26));
		k.insert(new Point2D(13, 31));
		k.insert(new Point2D(11, 39));
		k.insert(new Point2D(23, 36));
		k.insert(new Point2D(34, 40));
		k.insert(new Point2D(12, 37));
		k.insert(new Point2D(4, 14));
		k.insert(new Point2D(33, 38));
		k.draw();
		
//		String dirName = args[0];
//		File dir = new File(dirName);
//		if (dir.isDirectory()) {
//			String[] files = dir.list();
//			String file = "circle4.txt";
//			String file = files[0];
//			for (String file : files) {
				/*In in = new In(dirName + File.separator + file);
				KdTree t = new KdTree();
				while (!in.isEmpty()) {
					Point2D p = new Point2D(in.readDouble(), in.readDouble());
					t.insert(p);
				}
				System.out.println("file: " + file);
				System.out.println("size: " + t.size());
//				t.draw();
				
				Point2D n1 = new Point2D(0.5, 0);
				Point2D nearest = t.nearest(n1);
				System.out.println("n1 " + nearest);*/
				
				/*
				RectHV rect = new RectHV(0.57, 0.4, 0.75, 0.55);
				StdDraw.setPenColor(StdDraw.GREEN);
				rect.draw();
				Iterator<Point2D> i = t.range(rect).iterator();
				int c = 0;
				while (i.hasNext()) {
					System.out.println(i.next());
					c++;
				}
				System.out.println(c + " pontos no ret " + rect);
				*/
//			}
//		}
		/*
		
		
		KdTree tree = new KdTree();
		Point2D A = new Point2D(0.7, 0.2);
		Point2D B = new Point2D(0.5, 0.4);
		Point2D C = new Point2D(0.2, 0.3);
		Point2D D = new Point2D(0.4, 0.7);
		Point2D E = new Point2D(0.9, 0.6);*/
		
//		Point2D A = new Point2D(0.206107, 0.095492);
//		Point2D B = new Point2D(0.975528, 0.654508);
//		Point2D C = new Point2D(0.024472, 0.345492);
//		Point2D D = new Point2D(0.793893, 0.095492);
//		Point2D E = new Point2D(0.793893, 0.904508);
//		Point2D F = new Point2D(0.975528, 0.345492);
//		Point2D G = new Point2D(0.206107, 0.904508);
//		Point2D H = new Point2D(0.500000, 0.000000);
//		Point2D I = new Point2D(0.024472, 0.654508);
//		Point2D J = new Point2D(0.500000, 1.000000);
		
//		System.out.println(tree.contains(A));
		/*
		tree.insert(A);
		tree.insert(B);
		tree.insert(C);
		tree.insert(D);
		tree.insert(E);*/
//		tree.insert(F);
//		tree.insert(G);
//		tree.insert(H);
//		tree.insert(I);
//		tree.insert(J);
		/*Point2D fakeC = new Point2D(0.2, 0.1);
		System.out.println(tree.contains(fakeC));
		
		tree.draw();*/
		/*
		RectHV rect = new RectHV(0, 0, 0.69, 1);
		Iterator<Point2D> i = tree.range(rect).iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}*/
		/*
		Point2D p = new Point2D(1, 1);
		System.out.println("closest");
		System.out.println(tree.nearest(p));*/
	}
}
