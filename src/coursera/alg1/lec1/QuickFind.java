package coursera.alg1.lec1;

public class QuickFind {

	private int[] id;
	
	public QuickFind(int N) {
		id = new int[N];
		for (int i = 0; i < N; i++) {
			id[i] = i;
		}
	}
	
	public boolean connected(int p, int q) {
		return id[p] == id[q];
	}
	
	public void union(int p, int q) {
		int pid = id[p];
		int qid = id[q];
		for (int i = 0; i < id.length; i++) {
			if (id[i] == pid) {
				id[i] = qid;
			}
		}
	}
	
	public static void main(String[] args) {
		QuickFind q = new QuickFind(10);
		//8-7 7-6 8-0 2-3 1-7 6-9 
		q.union(8, 7);
		q.union(7, 6);
		q.union(8, 0);
		q.union(2, 3);
		q.union(1, 7);
		q.union(6, 9);
		
		for (int i = 0; i < q.id.length; i++) {
			System.out.print(q.id[i] + " ");
		}
	}
}
