package coursera.alg1.lec1;

public class QuickUnion {
	
	private int[] id;
	private int[] sz;
	
	public QuickUnion(int N) {
		id = new int[N];
		sz = new int[N];
		for (int i = 0; i < N; i++) {
			id[i] = i;
			sz[i] = 1;
		}
	}
	
	private int root(int i) {
		while (i != id[i]) {
			i = id[i];
		}
		return i;
	}
	
	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}
	
	public void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		if (i == j) return;
		if (sz[i] < sz[j]) {
			id[i] = j;
			sz[j] += sz[i];
		} else {
			id[j] = i;
			sz[i] += sz[j];
		}
	}
	
	public static void main(String[] args) {
		QuickUnion q = new QuickUnion(10);
		// 0-3 5-9 4-7 5-7 0-6 2-6 6-8 3-4 1-3 
		q.union(0, 3);
		q.union(5, 9);
		q.union(4, 7);
		q.union(5, 7);
		q.union(0, 6);
		q.union(2, 6);
		q.union(6, 8);
		q.union(3, 4);
		q.union(1, 3);
		
		for (int i = 0; i < q.id.length; i++) {
			System.out.print(q.id[i] + " ");
		}
	}
}
